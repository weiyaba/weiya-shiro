# weiya-shiro

#### 项目介绍


两年前，创建了喂呀开源项目，第一个项目就是权限管理系统，无奈的是，中途因个人原因停止了。
好在，这一切都过去了，又可以心无旁骛的回归代码本身，却发现这种开源项目已经泛滥。 
但是我还是想坚持始终，从最初的想法做起，善始善终，把这个项目完成。
我想，我唯一能改变的大概就是版本号了，哈哈。
本项目依赖jar包版本都是maven仓库最新的，期间踩了不少坑，最终也都爬出来了。
如果有人感兴趣，就clone或者download跑跑看，希望可以让你减少掉坑的可能。
如果爬不出来，欢迎加群[215162400](https://jq.qq.com/?_wv=1027&k=5hTsDhU)和[825747366](https://jq.qq.com/?_wv=1027&k=57mMRjY)交流。
如果你喜欢本项目，觉得还不错，轻轻点一下Watch和Star，给予我更多动力 :smile: 

#### 软件架构
1. Spring 5.0.7.RELEASE
2. 数据持久层框架Mybatis 3.4.6 
3. 权限框架使用Shiro 1.4.0
4. 缓存使用Jedis2.9.0 + Spring-data-redis2.0.8.RELEASE
5. 数据库Mysql版本 8.0.11
6. 分页插件com.github.pagehelper 5.1.4
7. mybatis generator第三方插件tk.mybatis
8. 数据库连接池druid 1.1.10
9. 验证码kaptcha
10. 前端使用adminlte模板，模板引擎Thymeleaf3.0.9.RELEASE



#### 安装教程

1. 使用IDEA，clone或者download本项目
2. 创建数据库，执行docs下sql文件，初始化数据库表，安装Redis，并将配置修改成自己的。
3. 运行项目，浏览器输入http://127.0.0.1:8080/ 或者 http://localhost:8080/
4. 项目预览地址:[喂呀权限系统shiro模块](http://shiro.weiyaba.cn/)
4. 超级管理员账号密码:super/123456
   普通用户账号密码：weiya/weiya
   Druid监控账号密码：weiya/weiya



#### 项目图片预览
![用户管理](https://images.gitee.com/uploads/images/2018/0727/170028_a60dd9d6_757982.png "屏幕截图.png")

![分配角色](https://images.gitee.com/uploads/images/2018/0727/170209_ad453f39_757982.png "屏幕截图.png")

![角色管理](https://images.gitee.com/uploads/images/2018/0727/170134_0090adb8_757982.png "屏幕截图.png")

![分配权限](https://images.gitee.com/uploads/images/2018/0727/170248_0e1a10ea_757982.png "屏幕截图.png")

![资源管理](https://images.gitee.com/uploads/images/2018/0727/170311_50038226_757982.png "屏幕截图.png")

![新增资源](https://images.gitee.com/uploads/images/2018/0727/170342_0eb2e485_757982.png "屏幕截图.png")

![Druid监控](https://images.gitee.com/uploads/images/2018/0727/170409_332f81f7_757982.png "屏幕截图.png")

![图标工具](https://images.gitee.com/uploads/images/2018/0727/170602_b9a34584_757982.png "屏幕截图.png")

![一键布局](https://images.gitee.com/uploads/images/2018/0727/170645_8daa06df_757982.png "屏幕截图.png")


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)