package com.weiya.response;

import org.apache.http.HttpStatus;

/**
 * <p class="detail">
 * 功能:系统常量
 * </p>
 *
 * @author BaoWeiwei
 * @ClassName System const.
 * @Version V1.0.
 * @date 2018.07.17 06:47:36
 */
public class SystemConst {
    /**
     * 数据状态：1有效；2无效；3已删除
     */
    public static final Integer DATA_VALID = 1;
    public static final Integer DATA_INVALID = 2;
    public static final Integer DATA_DELETED = 3;

    public static String TOP_MENU_ID = String.valueOf(0);
    public static String TOP_MENU_NAME = "顶层菜单";

    public static final String SHIRO_CACHE_PREFIX = "weiya-cache:";
    public static final String SHIRO_CACHE_PREFIX_KEYS = "weiya-cache:*";
    public static final String SHIRO_SESSION_PREFIX = "weiya-session:";
    public static final String SHIRO_SESSION_PREFIX_KEYS = "weiya-session:*";

    /**
     * 语言
     * en：英文
     * zh：中文
     */
    public static final String LANG_EN = "en";
    public static final String LANG_ZH = "zh";
}
