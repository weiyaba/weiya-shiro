package com.weiya.response;

/**
 * <p class="detail">
 * 功能:分页工具类
 * </p>
 *
 * @author BaoWeiwei
 * @ClassName Page util.
 * @Version V1.0.
 * @date 2018.07.22 23:13:16
 */
public class PageUtil {
    /**
     * <p class="detail">
     * 功能:计算pageNo
     * </p>
     *
     * @param limit  :
     * @param offset :
     * @return integer
     * @author BaoWeiwei
     * @date 2018.07.22 23:13:16
     */
    public static Integer getPageNo(Integer limit,Integer offset){
        return offset==0 ? 1 : offset / limit + 1;
    }
}
