package com.weiya.response;

import java.util.List;

/**
 * <p class="detail">
 * 功能:自定义ajax请求返回结果工具类
 * </p>
 *
 * @author BaoWeiwei
 * @ClassName Response util.
 * @Version V1.0.
 * @date 2018.07.16 17:53:54
 */
public class ResponseUtil {
    public static ResponseVo success() {
        return response(ResponseCode.SUCCESS.getCode(), null, null);
    }

    public static ResponseVo success(String msg) {
        return response(ResponseCode.SUCCESS.getCode(), msg, null);
    }

    public static ResponseVo success(String msg, Object data) {
        return response(ResponseCode.SUCCESS.getCode(), msg, data);
    }

    public static ResponseVo error() {
        return response(ResponseCode.ERROR.getCode(), null, null);
    }

    public static ResponseVo error(String msg) {
        return response(ResponseCode.ERROR.getCode(), msg, null);
    }

    public static ResponseVo error(Integer code,String msg) {
        return response(code, msg, null);
    }

    public static ResponseVo error(Integer code,String msg, Object data) {
        return response(code, msg, data);
    }

    public static ResponseVo response(Integer code, String message, Object data) {
        return new ResponseVo<>(code, message, data);
    }

    public static PageResultVo table(List<?> list, Long total) {
        return new PageResultVo(list, total);
    }
}
