package com.weiya.response;

import java.io.Serializable;

/**
 * <p class="detail">
 * 功能:自定义ajax请求返回结果
 * </p>
 *
 * @param <T> the type parameter
 * @author BaoWeiwei
 * @ClassName Response vo.
 * @Version V1.0.
 * @date 2018.07.16 17:52:51
 */
public class ResponseVo<T> implements Serializable {

    /**
     * 状态码
     */
    private Integer code;
    /**
     * 消息（错误信息）
     */
    private String msg;
    /**
     * 返回数据
     */
    private T data;

    public ResponseVo() {

    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public ResponseVo(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}
