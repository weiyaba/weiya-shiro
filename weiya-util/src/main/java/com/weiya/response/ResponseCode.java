package com.weiya.response;

/**
 * <p class="detail">
 * 功能:自定义返回状态码
 * </p>
 *
 * @author BaoWeiwei
 * @ClassName Response code.
 * @Version V1.0.
 * @date 2018.07.27 13:36:15
 */
public enum ResponseCode {
    SUCCESS(200, "操作成功!"),
    ERROR(500, "服务器错误!"),
    VERIFY_CAPTCHA_ERROR(50001, "验证码错误!"),
    USERNAME_ALREADY_EXIST(50002, "用户名已存在!"),
    PASSWPRD_DIFFERENT(50003, "密码不一致，请检查!"),
    REGISTER_FAIL(50004, "注册失败!"),
    USERNAME_OR_PASSWORD_ERROR(50005, "用户名或者密码错误!"),
    FORBIDDEN_ACCOUNT(50006, "账户已禁用,请联系管理员!"),
    LOCKED_ACCOUNT(50007, "账户已锁定,请联系管理员!"),
    UNKNOWN_ACCOUNT(50008, "账户不存在!"),
    UNAUTHORIZED(401, "未授权,无法访问!");

    private int code;
    private String msg;

    ResponseCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "ResponseCode{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                '}';
    }
}
