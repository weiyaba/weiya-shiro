package com.weiya.response;

import java.util.List;


/**
 * <p class="detail">
 * 功能:分页返回结果封装
 * </p>
 *
 * @author BaoWeiwei
 * @ClassName Page result vo.
 * @Version V1.0.
 * @date 2018.07.22 23:10:30
 */
public class PageResultVo {
    private List rows;
    private Long total;

    /**
     * Gets rows.
     *
     * @return the rows
     */
    public List getRows() {
        return rows;
    }

    /**
     * Sets rows.
     *
     * @param rows the rows
     */
    public void setRows(List rows) {
        this.rows = rows;
    }

    /**
     * Gets total.
     *
     * @return the total
     */
    public Long getTotal() {
        return total;
    }

    /**
     * Sets total.
     *
     * @param total the total
     */
    public void setTotal(Long total) {
        this.total = total;
    }

    /**
     * Instantiates a new Page result vo.
     *
     * @param rows  the rows
     * @param total the total
     */
    public PageResultVo(List rows , Long total) {
        this.total = total;
        this.rows = rows;
    }

}
