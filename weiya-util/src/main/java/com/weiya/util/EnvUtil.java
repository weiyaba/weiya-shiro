package com.weiya.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import java.io.IOException;
import java.util.Properties;

/**
 * <p class="detail">
 * 功能:属性文件工具类,可以通过配置扫描到不同properties的属性 扫描classpath*:/*.properties下的配置文件,也可以在配置文件自定义设置
 * </p>
 *
 * @author BaoWeiwei
 * @ClassName Env utils.
 * @Version V1.0.
 * @date 2017.04.26 22:30:09
 */
public class EnvUtil extends PropertySourcesPlaceholderConfigurer {
    private static Logger logger = LoggerFactory.getLogger(EnvUtil.class);
    /**
     * 存取properties配置文件key-value结果
     */
    private static Properties prop;

    public static String getProperty(String key) {
        return prop.getProperty(key);
    }

    public void init() throws IOException {
        try {
            // 即使多次实例化,只合并一次
            if(prop == null){
                prop = super.mergeProperties();
            }
        } catch (IOException e) {
            logger.error("load properties error", e);
            throw new IOException("load properties error");
        }
    }
}
