package com.weiya.util;

import java.util.UUID;

/**
 * <p class="detail">
 * 功能:生成随机uuid值
 * </p>
 *
 * @author BaoWeiwei
 * @ClassName Uuid util.
 * @Version V1.0.
 * @date 2018.07.13 10:23:15
 */
public class UUIDUtil {


    /**
     * Gets unique id by uu id.
     *
     * @return the unique id by uu id
     */
    public static String getUniqueIdByUUId() {
        int machineId = 1;//最大支持1-9个集群机器部署
        int hashCodeV = UUID.randomUUID().toString().hashCode();
        if(hashCodeV < 0) {//有可能是负数
            hashCodeV = - hashCodeV;
        }
        // 0 代表前面补充0
        // 4 代表长度为4
        // d 代表参数为正数型
        return machineId + String.format("%015d", hashCodeV);
    }


    /**
     * The constant chars.
     */
    public static String[] chars = new String[] { "a", "b", "c", "d", "e", "f",
            "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
            "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",
            "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z" };


    /**
     * <p class="detail">
     * 功能:
     * </p>
     *
     * @return string
     * @author BaoWeiwei
     * @date 2018.07.13 10:23:15
     */
    public static String generateShortUuid() {
        StringBuffer shortBuffer = new StringBuffer();
        String uuid = UUID.randomUUID().toString().replace("-", "");
        for (int i = 0; i < 8; i++) {
            String str = uuid.substring(i * 4, i * 4 + 4);
            int x = Integer.parseInt(str, 16);
            shortBuffer.append(chars[x % 0x3E]);
        }
        return shortBuffer.toString();

    }

    /**
     * 主程序入口.
     *
     * @param args :
     * @author BaoWeiwei
     * @date 2018.07.13 10:23:15
     */
    public static void main(String[] args) {
        System.out.println(getUniqueIdByUUId());
        System.out.println(generateShortUuid());
    }


}
