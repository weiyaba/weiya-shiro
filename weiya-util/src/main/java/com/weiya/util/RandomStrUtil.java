package com.weiya.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Andy on 2017/1/10.
 *
 * 生成随机数工具类
 *
 * @author BaoWeiwei
 * @ClassName Randomstr util.
 * @Version V1.0.
 * @date 2017.01.10 14:00:14
 */
public class RandomStrUtil {


    public static Set<Integer> generatorId = new HashSet<Integer>();

    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

    public static String randomStr(Set<Integer> set){

        int a;
        do {
            a = (int) (Math.random() * 1000000);
        } while (set.contains(a));
        set.add(a);
        return (a + 1000000 + "").substring(1);

    }

    public static String generatorId(){

        return sdf.format(new Date())+randomStr(generatorId);
    }

    public static void main(String[] args) {
        System.out.println(RandomStrUtil.generatorId());
    }
}
