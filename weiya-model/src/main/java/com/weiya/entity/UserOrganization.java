package com.weiya.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "user_organization")
public class UserOrganization implements Serializable {
    private static final long serialVersionUID = 2186663860828388599L;
    @Id
    @Column(name = "id")
    private Integer id;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private String userId;

    /**
     * 组织id
     */
    @Column(name = "organization_id")
    private String organizationId;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取用户id
     *
     * @return user_id - 用户id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 获取组织id
     *
     * @return organization_id - 组织id
     */
    public String getOrganizationId() {
        return organizationId;
    }

    /**
     * 设置组织id
     *
     * @param organizationId 组织id
     */
    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }
}