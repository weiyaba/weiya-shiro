package com.weiya.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Table(name = "organization")
public class Organization implements Serializable {
    private static final long serialVersionUID = -8323758356070933235L;
    /**
     * 组织id
     */
    @Id
    @Column(name = "organization_id")
    private String organizationId;

    /**
     * 组织名称
     */
    @Column(name = "name")
    private String name;

    /**
     * 组织职能描述
     */
    @Column(name = "description")
    private String description;

    /**
     * 父级组织id
     */
    @Column(name = "parent_id")
    private Integer parentId;

    /**
     * 组织编号
     */
    @Column(name = "code")
    private String code;

    /**
     * 状态：1有效；2删除
     */
    @Column(name = "status")
    private Integer status;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 获取组织id
     *
     * @return organization_id - 组织id
     */
    public String getOrganizationId() {
        return organizationId;
    }

    /**
     * 设置组织id
     *
     * @param organizationId 组织id
     */
    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * 获取组织名称
     *
     * @return name - 组织名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置组织名称
     *
     * @param name 组织名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取组织职能描述
     *
     * @return description - 组织职能描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置组织职能描述
     *
     * @param description 组织职能描述
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 获取父级组织id
     *
     * @return parent_id - 父级组织id
     */
    public Integer getParentId() {
        return parentId;
    }

    /**
     * 设置父级组织id
     *
     * @param parentId 父级组织id
     */
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    /**
     * 获取组织编号
     *
     * @return code - 组织编号
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置组织编号
     *
     * @param code 组织编号
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取状态：1有效；2删除
     *
     * @return status - 状态：1有效；2删除
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置状态：1有效；2删除
     *
     * @param status 状态：1有效；2删除
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}