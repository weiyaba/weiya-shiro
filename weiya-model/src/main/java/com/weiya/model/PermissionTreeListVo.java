package com.weiya.model;


/**
 * <p class="detail">
 * 功能:分配权限ztree格式封装
 * </p>
 *
 * @author BaoWeiwei
 * @ClassName Permission tree list vo.
 * @Version V1.0.
 * @date 2018.07.24 16:30:11
 */
public class PermissionTreeListVo {
    private String permissionId;
    private String name;
    private String parentId;
    private Boolean open=true;
    private Boolean checked=false;

    public String getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Boolean getOpen() {
        return open;
    }

    public void setOpen(Boolean open) {
        this.open = open;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    @Override
    public String toString() {
        return "PermissionTreeListVo{" +
                "permissionId='" + permissionId + '\'' +
                ", name='" + name + '\'' +
                ", parentId=" + parentId +
                ", open=" + open +
                ", checked=" + checked +
                '}';
    }
}
