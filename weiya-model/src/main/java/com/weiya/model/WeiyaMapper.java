package com.weiya.model;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * <p class="detail">
 * 功能:自定义mapper，让自动生成的mapper接口继承该接口
 * </p>
 *
 * @author Baoweiwei
 * @ClassName Weiya mapper.
 * @Version V1.0.
 * @date 2018.07.15 23:58:41
 */
public interface WeiyaMapper<T> extends Mapper<T>, MySqlMapper<T> {
    //TODO
    //FIXME 特别注意，该接口不能被扫描到，否则会出错
}
