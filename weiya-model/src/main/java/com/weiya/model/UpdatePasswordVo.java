package com.weiya.model;

/**
 * <p class="detail">
 * 功能:修改密码封装VO
 * </p>
 *
 * @author BaoWeiwei
 * @ClassName Update password vo.
 * @Version V1.0.
 * @date 2018.07.25 13:33:20
 */
public class UpdatePasswordVo {
    String oldPassword;
    String newPassword;
    String confirmNewPassword;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmNewPassword() {
        return confirmNewPassword;
    }

    public void setConfirmNewPassword(String confirmNewPassword) {
        this.confirmNewPassword = confirmNewPassword;
    }

    @Override
    public String toString() {
        return "UpdatePasswordVo{" +
                "oldPassword='" + oldPassword + '\'' +
                ", newPassword='" + newPassword + '\'' +
                ", confirmNewPassword='" + confirmNewPassword + '\'' +
                '}';
    }


}
