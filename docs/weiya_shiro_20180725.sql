/*
 Navicat Premium Data Transfer

 Source Server         : weiya_localhost
 Source Server Type    : MySQL
 Source Server Version : 80011
 Source Host           : localhost:3306
 Source Schema         : weiya_shiro

 Target Server Type    : MySQL
 Target Server Version : 80011
 File Encoding         : 65001

 Date: 25/07/2018 14:45:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for organization
-- ----------------------------
DROP TABLE IF EXISTS `organization`;
CREATE TABLE `organization`  (
  `organization_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组织id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组织名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组织职能描述',
  `parent_id` int(11) NULL DEFAULT NULL COMMENT '父级组织id',
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组织编号',
  `status` int(1) NOT NULL COMMENT '状态：1有效；2删除',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`organization_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of organization
-- ----------------------------
INSERT INTO `organization` VALUES ('2017', '喂呀', '喂呀码吧', 0, 'WEIYA', 1, '2017-06-27 15:00:21', '2017-06-27 15:00:24');
INSERT INTO `organization` VALUES ('2017001', '行政部', '负责公司日常管理工作', 2017, 'XZB', 1, '2017-06-28 23:37:20', '2017-06-28 23:37:23');
INSERT INTO `organization` VALUES ('2017002', '财务部', '负责公司财务管理', 2017, 'CWB', 1, '2017-06-28 23:39:37', '2017-06-30 23:39:41');
INSERT INTO `organization` VALUES ('2017003', '技术部', '负责公司技术支撑', 2017, 'JSB', 1, '2017-06-30 23:40:10', '2017-06-30 23:40:12');
INSERT INTO `organization` VALUES ('201700301', '开发组', '开发人员', 2017003, 'JSB_KFZ', 1, '2017-06-30 23:40:39', '2017-06-30 23:40:42');
INSERT INTO `organization` VALUES ('201700302', '测试组', '测试人员', 2017003, 'JSB_CSZ', 1, '2017-06-30 23:41:20', '2017-06-30 23:41:23');

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission`  (
  `permission_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限id',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限描述',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限访问路径',
  `perms` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `parent_id` int(11) NULL DEFAULT NULL COMMENT '父级权限id',
  `type` int(1) NULL DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `order_num` int(3) NULL DEFAULT 0 COMMENT '排序',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `status` int(1) NOT NULL COMMENT '状态：1有效；2删除',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`permission_id`) USING BTREE,
  INDEX `permission_id`(`permission_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES ('1', '首页', '首页', '/overview', 'overview', 0, 1, 1, 'fa fa-home', 1, '2017-09-27 21:22:02', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('2', '权限管理', '权限管理', NULL, NULL, 0, 0, 2, 'fa fa-th-list', 1, '2017-07-13 15:04:42', '2018-07-25 10:00:54');
INSERT INTO `permission` VALUES ('201', '用户管理', '用户管理', '/users', 'users', 2, 1, 1, 'fa fa-circle-o', 1, '2017-07-13 15:05:47', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('20101', '列表查询', '用户列表查询', '/user/list', 'user:list', 201, 2, 0, NULL, 1, '2017-07-13 15:09:24', '2017-10-09 05:38:29');
INSERT INTO `permission` VALUES ('20102', '新增', '新增用户', '/user/add', 'user:add', 201, 2, 0, NULL, 1, '2017-07-13 15:06:50', '2018-02-28 17:58:46');
INSERT INTO `permission` VALUES ('20103', '编辑', '编辑用户', '/user/edit', 'user:edit', 201, 2, 0, NULL, 1, '2017-07-13 15:08:03', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('20104', '删除', '删除用户', '/user/delete', 'user:delete', 201, 2, 0, NULL, 1, '2017-07-13 15:08:42', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('20105', '批量删除', '批量删除用户', '/user/batch/delete', 'user:batchDelete', 201, 2, 0, '', 1, '2018-07-11 01:53:09', '2018-07-11 01:53:09');
INSERT INTO `permission` VALUES ('20106', '分配角色', '分配角色', '/user/assign/role', 'user:assignRole', 201, 2, 0, NULL, 1, '2017-07-13 15:09:24', '2017-10-09 05:38:29');
INSERT INTO `permission` VALUES ('202', '角色管理', '角色管理', '/roles', 'roles', 2, 1, 2, 'fa fa-circle-o', 1, '2017-07-17 14:39:09', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('20201', '列表查询', '角色列表查询', '/role/list', 'role:list', 202, 2, 0, NULL, 1, '2017-10-10 15:31:36', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('20202', '新增', '新增角色', '/role/add', 'role:add', 202, 2, 0, NULL, 1, '2017-07-17 14:39:46', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('20203', '编辑', '编辑角色', '/role/edit', 'role:edit', 202, 2, 0, NULL, 1, '2017-07-17 14:40:15', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('20204', '删除', '删除角色', '/role/delete', 'role:delete', 202, 2, 0, NULL, 1, '2017-07-17 14:40:57', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('20205', '批量删除', '批量删除角色', '/role/batch/delete', 'role:batchDelete', 202, 2, 0, '', 1, '2018-07-10 22:20:43', '2018-07-10 22:20:43');
INSERT INTO `permission` VALUES ('20206', '分配权限', '分配权限', '/role/assign/permission', 'role:assignPerms', 202, 2, 0, NULL, 1, '2017-09-26 07:33:05', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('203', '资源管理', '资源管理', '/permissions', 'permissions', 2, 1, 3, 'fa fa-circle-o', 1, '2017-09-26 07:33:51', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('20301', '列表查询', '资源列表', '/permission/list', 'permission:list', 203, 2, 0, NULL, 1, '2018-07-12 16:25:28', '2018-07-12 16:25:33');
INSERT INTO `permission` VALUES ('20302', '新增', '新增资源', '/permission/add', 'permission:add', 203, 2, 0, NULL, 1, '2017-09-26 08:06:58', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('20303', '编辑', '编辑资源', '/permission/edit', 'permission:edit', 203, 2, 0, NULL, 1, '2017-09-27 21:29:04', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('20304', '删除', '删除资源', '/permission/delete', 'permission:delete', 203, 2, 0, NULL, 1, '2017-09-27 21:29:50', '2018-02-27 10:53:14');
INSERT INTO `permission` VALUES ('3', '运维管理', '运维管理', '', NULL, 0, 0, 3, 'fa fa-th-list', 1, '2018-07-06 15:19:26', '2018-07-06 15:19:26');
INSERT INTO `permission` VALUES ('301', '数据监控', '数据监控', '/database/monitoring', 'database', 3, 1, 1, 'fa fa-circle-o', 1, '2018-07-06 15:19:55', '2018-07-06 15:19:55');
INSERT INTO `permission` VALUES ('4', '系统工具', '系统工具', '', NULL, 0, 0, 4, 'fa fa-th-list', 1, '2018-07-06 15:20:38', '2018-07-06 15:20:38');
INSERT INTO `permission` VALUES ('401', '图标工具', '图标工具', '/icons', 'icons', 4, 1, 1, 'fa fa-circle-o', 1, '2018-07-06 15:21:00', '2018-07-06 15:21:00');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `role_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色id',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  `status` int(1) NOT NULL COMMENT '状态：1有效；2无效；3已删除',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`role_id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', '超级管理员', '超级管理员', 1, '2017-06-28 20:30:05', '2018-07-24 16:22:03');
INSERT INTO `role` VALUES ('1000001199449754', '文章管理员', '发布文章', 1, '2018-07-24 14:24:47', '2018-07-24 16:19:22');
INSERT INTO `role` VALUES ('2', '管理员', '管理员', 1, '2017-06-30 23:35:19', '2018-07-24 16:22:27');
INSERT INTO `role` VALUES ('3', '普通用户', '普通用户', 1, '2017-06-30 23:35:44', '2018-07-24 16:22:03');
INSERT INTO `role` VALUES ('4', '数据库管理员', '数据库管理员', 1, '2017-07-12 11:50:22', '2018-07-24 16:21:49');

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色id',
  `permission_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 222 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_permission
-- ----------------------------
INSERT INTO `role_permission` VALUES (100, '4', '1');
INSERT INTO `role_permission` VALUES (101, '4', '3');
INSERT INTO `role_permission` VALUES (102, '4', '301');
INSERT INTO `role_permission` VALUES (158, '1', '1');
INSERT INTO `role_permission` VALUES (159, '1', '2');
INSERT INTO `role_permission` VALUES (160, '1', '201');
INSERT INTO `role_permission` VALUES (161, '1', '20101');
INSERT INTO `role_permission` VALUES (162, '1', '20102');
INSERT INTO `role_permission` VALUES (163, '1', '20103');
INSERT INTO `role_permission` VALUES (164, '1', '20104');
INSERT INTO `role_permission` VALUES (165, '1', '20105');
INSERT INTO `role_permission` VALUES (166, '1', '20106');
INSERT INTO `role_permission` VALUES (167, '1', '20201');
INSERT INTO `role_permission` VALUES (168, '1', '202');
INSERT INTO `role_permission` VALUES (169, '1', '20202');
INSERT INTO `role_permission` VALUES (170, '1', '20203');
INSERT INTO `role_permission` VALUES (171, '1', '20204');
INSERT INTO `role_permission` VALUES (172, '1', '20205');
INSERT INTO `role_permission` VALUES (173, '1', '20206');
INSERT INTO `role_permission` VALUES (174, '1', '203');
INSERT INTO `role_permission` VALUES (175, '1', '20301');
INSERT INTO `role_permission` VALUES (176, '1', '20302');
INSERT INTO `role_permission` VALUES (177, '1', '20303');
INSERT INTO `role_permission` VALUES (178, '1', '20304');
INSERT INTO `role_permission` VALUES (179, '1', '3');
INSERT INTO `role_permission` VALUES (180, '1', '301');
INSERT INTO `role_permission` VALUES (181, '1', '4');
INSERT INTO `role_permission` VALUES (182, '1', '401');
INSERT INTO `role_permission` VALUES (183, '2', '1');
INSERT INTO `role_permission` VALUES (184, '2', '2');
INSERT INTO `role_permission` VALUES (185, '2', '201');
INSERT INTO `role_permission` VALUES (186, '2', '20101');
INSERT INTO `role_permission` VALUES (187, '2', '20102');
INSERT INTO `role_permission` VALUES (188, '2', '20103');
INSERT INTO `role_permission` VALUES (189, '2', '20104');
INSERT INTO `role_permission` VALUES (190, '2', '20105');
INSERT INTO `role_permission` VALUES (191, '2', '20106');
INSERT INTO `role_permission` VALUES (192, '2', '20201');
INSERT INTO `role_permission` VALUES (193, '2', '202');
INSERT INTO `role_permission` VALUES (194, '2', '20202');
INSERT INTO `role_permission` VALUES (195, '2', '20203');
INSERT INTO `role_permission` VALUES (196, '2', '20204');
INSERT INTO `role_permission` VALUES (197, '2', '20205');
INSERT INTO `role_permission` VALUES (198, '2', '20206');
INSERT INTO `role_permission` VALUES (199, '2', '203');
INSERT INTO `role_permission` VALUES (200, '2', '20301');
INSERT INTO `role_permission` VALUES (201, '2', '20302');
INSERT INTO `role_permission` VALUES (202, '2', '20303');
INSERT INTO `role_permission` VALUES (203, '2', '20304');
INSERT INTO `role_permission` VALUES (212, '3', '1');
INSERT INTO `role_permission` VALUES (213, '3', '2');
INSERT INTO `role_permission` VALUES (214, '3', '201');
INSERT INTO `role_permission` VALUES (215, '3', '20101');
INSERT INTO `role_permission` VALUES (216, '3', '202');
INSERT INTO `role_permission` VALUES (217, '3', '20201');
INSERT INTO `role_permission` VALUES (218, '3', '203');
INSERT INTO `role_permission` VALUES (219, '3', '20301');
INSERT INTO `role_permission` VALUES (220, '3', '4');
INSERT INTO `role_permission` VALUES (221, '3', '401');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `salt` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '加密盐值',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  `sex` int(255) NULL DEFAULT NULL COMMENT '性别：1男2女',
  `age` int(3) NULL DEFAULT NULL COMMENT '年龄',
  `organization_id` int(11) NULL DEFAULT NULL COMMENT '组织id',
  `status` int(1) NOT NULL COMMENT '用户状态：1启用; 2禁用；3已删除',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `last_login_time` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1000000219123874', 'data', '5fa8d922d6e2ef1f4b21d428f212324d', '257ba293014f1c1eefb84cd194e5c673', '', '', 0, NULL, NULL, 1, '2018-07-25 14:37:47', '2018-07-25 14:37:47', '2018-07-25 14:37:47');
INSERT INTO `user` VALUES ('1000000641948901', 'weiya', '723d3dc4e5e624a5bd56f4427b0eeb2d', '59b09520045c1154949fcf6c27706067', 'weiya@weiya.com', '', 0, NULL, NULL, 1, '2018-07-25 14:36:13', '2018-07-25 14:36:13', '2018-07-25 14:43:01');
INSERT INTO `user` VALUES ('1000001208197843', 'admin', 'd117ec6e0c24b427d02eb8b2e3ef5a56', '5c0e3b9209c0e6a3ea6d1881da63705a', 'admin@weiya.com', '13000112233', 1, 23, NULL, 1, '2018-07-18 03:53:32', '2018-07-25 13:54:38', '2018-07-25 13:56:01');
INSERT INTO `user` VALUES ('1000002003992646', 'super', '76021404cb95ddd2396f500a809b6906', 'b571d8fc6d867f1b239c3cbb6e264a13', 'super@weiya.com', '13812345678', 1, 30, NULL, 1, '2018-07-23 17:03:08', '2018-07-24 10:20:38', '2018-07-25 14:34:43');

-- ----------------------------
-- Table structure for user_organization
-- ----------------------------
DROP TABLE IF EXISTS `user_organization`;
CREATE TABLE `user_organization`  (
  `id` int(11) NOT NULL,
  `user_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `organization_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组织id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `role_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES (21, '1000000081095475', '4');
INSERT INTO `user_role` VALUES (22, '1000000641948901', '3');
INSERT INTO `user_role` VALUES (24, '1000001208197843', '2');
INSERT INTO `user_role` VALUES (25, '1000002003992646', '1');
INSERT INTO `user_role` VALUES (26, '1000000219123874', '4');

SET FOREIGN_KEY_CHECKS = 1;
