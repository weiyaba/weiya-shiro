package com.weiya.service;

import com.weiya.entity.Permission;
import com.weiya.entity.Role;
import com.weiya.entity.User;
import com.weiya.response.ResponseVo;

import java.util.List;
import java.util.Set;

/**
 * <p class="detail">
 * 功能:角色业务接口
 * </p>
 *
 * @author Baoweiwei
 * @ClassName Role service.
 * @Version V1.0.
 * @date 2018.07.17 23:12:14
 */
public interface RoleService {
    /**
     * 根据用户id查询当前用户角色
     * @param userId
     * @return
     */
    Set<String> findRoleByUserId(String userId);

    /**
     * 根据条件查询角色列表
     * @param role
     * @return
     */
    List<Role> selectRoles(Role role);

    /**
     * 新增角色
     * @param role
     * @return
     */
    int insert(Role role);

    /**
     * 根据角色id查询角色详情
     * @param roleId
     * @return
     */
    Role findById(String roleId);

    /**
     * 修改角色信息
     * @param role
     * @return
     */
    int updateRole(Role role);

    /**
     * 批量删除角色
     * @param roleIdsList
     * @param status
     * @return
     */
    int updateStatusBatch(List<String> roleIdsList, Integer status);

    /**
     * 查询当前角色所拥有的权限
     * @param roleId
     * @return
     */
    List<Permission> findPermissionsByRoleId(String roleId);

    /**
     * 根据角色id分配权限
     * @param roleId
     * @param permissionIdsList
     * @return
     */
    ResponseVo addAssignPermission(String roleId, List<String> permissionIdsList);

    /**
     * 根据角色id查询用户当前角色的所有用户
     * @param roleId
     * @return
     */
    List<User> findByRoleId(String roleId);
}
