package com.weiya.service;

import com.weiya.entity.Permission;

import java.util.List;
import java.util.Set;

/**
 * <p class="detail">
 * 功能:权限业务接口
 * </p>
 *
 * @author Baoweiwei
 * @ClassName Permission service.
 * @Version V1.0.
 * @date 2018.07.17 23:12:17
 */
public interface PermissionService {
    /**
     * 根据用户id查询当前用户拥有权限资源
     * @param userId
     * @return
     */
    Set<String> findPermsByUserId(String userId);

    /**
     * 根据用户id查询当前用户拥有的菜单权限
     * @param userId
     * @return
     */
    List<Permission> selectMenuByUserId(String userId);

    /**
     * 查询所有有效资源
     * @param status
     * @return
     */
    List<Permission> selectAll(Integer status);

    /**
     * 查询
     * @param status
     * @return
     */
    List<Permission> selectAllMenuName(Integer status);

    /**
     * 根据id获取资源信息
     * @param permissionId
     * @return
     */
    Permission findByPermissionId(String permissionId);

    /**
     * 修改资源
     * @param permission
     * @return
     */
    int updateByPermissionId(Permission permission);

    /**
     * 根据资源id查询下级资源
     * @param permissionId
     * @return
     */
    int selectSubPermsByPermissionId(String permissionId);

    /**
     * 删除资源，状态置为已删除
     * @param permissionId
     * @param status
     * @return
     */
    int updateStatus(String permissionId, Integer status);

    /**
     * 新增资源
     * @param permission
     * @return
     */
    int insert(Permission permission);

    /**
     * 根据parentID查询当前目录下的所有资源
     * @param parentId
     * @return
     */
    List<Permission> selectAllPermission(String parentId);
}
