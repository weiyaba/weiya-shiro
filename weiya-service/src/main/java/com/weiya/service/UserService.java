package com.weiya.service;

import com.weiya.entity.User;
import com.weiya.response.ResponseVo;

import java.util.List;

/**
 * <p class="detail">
 * 功能:用户service
 * </p>
 *
 * @author Baoweiwei
 * @ClassName User service.
 * @Version V1.0.
 * @date 2018.07.17 07:16:15
 */
public interface UserService {

    /**
     * 根据用户名查询用户
     * @param username
     * @return
     */
    User selectByUsername(String username);

    /**
     * 注册用户
     * @param user
     * @return
     */
    int register(User user);

    /**
     * 更新最后登录时间
     * @param principal
     */
    void updateLastLoginTime(User principal);

    /**
     * 根据搜索条件查询所有用户
     * @param user
     * @return
     */
    List<User> selectUsers(User user);

    /**
     * 根据id批量删除用户
     * @param userIdsList
     * @param status
     * @return
     */
    int updateStatusBatch(List<String> userIdsList, Integer status);

    /**
     * 根据用户id查询用户详情
     * @param userId
     * @return user
     */
    User selectByUserId(String userId);

    /**
     * 根据用户id更新用户信息
     * @param user
     * @return int
     */
    int updateByUserId(User user);

    /**
     * 给用户分配角色
     * @param userId
     * @param roleIdsList
     * @return
     */
    ResponseVo addAssignRole(String userId, List<String> roleIdsList);

    /**
     * 修改用户信息
     * @param newUser
     * @return
     */
    int updateUserByPrimaryKey(User newUser);
}
