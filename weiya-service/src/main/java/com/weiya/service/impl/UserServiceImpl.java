package com.weiya.service.impl;

import com.weiya.entity.User;
import com.weiya.entity.UserRole;
import com.weiya.mapper.UserMapper;
import com.weiya.mapper.UserRoleMapper;
import com.weiya.response.ResponseUtil;
import com.weiya.response.ResponseVo;
import com.weiya.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p class="detail">
 * 功能:用户相关业务实现类
 * </p>
 *
 * @author BaoWeiwei
 * @ClassName User service.
 * @Version V1.0.
 * @date 2018.07.17 07:16:36
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRoleMapper userRoleMapper;

    /**
     * 根据用户名查询用户
     * @param username
     * @return
     */
    @Override
    public User selectByUsername(String username) {
        return userMapper.selectByUsername(username);
    }

    /**
     * 注册用户
     * @param user
     * @return
     */
    @Override
    public int register(User user) {
        return userMapper.insert(user);
    }

    /**
     * 更新最后登录时间
     * @param user
     */
    @Override
    public void updateLastLoginTime(User user) {
        userMapper.updateLastLoginTime(user);
    }

    /**
     * 根据搜索条件查询所有用户
     *
     * @param user
     * @return
     */
    @Override
    public List<User> selectUsers(User user) {
        return userMapper.selectUsers(user);
    }

    /**
     * 根据id批量删除用户
     *
     * @param userIdsList
     * @param status
     * @return
     */
    @Override
    public int updateStatusBatch(List<String> userIdsList, Integer status) {
        Map<String,Object> params = new HashMap<String,Object>(2);
        params.put("userIds",userIdsList);
        params.put("status",status);
        return userMapper.updateStatusBatch(params);
    }

    /**
     * 根据用户id查询用户详情
     *
     * @param userId
     * @return user
     */
    @Override
    public User selectByUserId(String userId) {
        return userMapper.selectByUserId(userId);
    }

    /**
     * 根据用户id更新用户信息
     *
     * @param user
     * @return int
     */
    @Override
    public int updateByUserId(User user) {
        return userMapper.updateByUserId(user);
    }

    /**
     * 给用户分配角色
     *
     * @param userId
     * @param roleIdsList
     * @return
     */
    @Override
    public ResponseVo addAssignRole(String userId, List<String> roleIdsList) {
        try{
            UserRole userRole = new UserRole();
            userRole.setUserId(userId);
            userRoleMapper.delete(userRole);
            for(String roleId :roleIdsList){
                if(roleId != ""){
                    userRole.setRoleId(roleId);
                    userRoleMapper.insert(userRole);
                }
            }
            return ResponseUtil.success("角色分配成功");
        }catch(Exception e){
            return ResponseUtil.error("分配角色失败");
        }
    }

    /**
     * 修改用户信息
     *
     * @param newUser
     * @return
     */
    @Override
    public int updateUserByPrimaryKey(User newUser) {
        return userMapper.updateByPrimaryKey(newUser);
    }
}
