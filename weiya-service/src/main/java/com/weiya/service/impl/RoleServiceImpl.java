package com.weiya.service.impl;

import com.weiya.entity.Permission;
import com.weiya.entity.Role;
import com.weiya.entity.RolePermission;
import com.weiya.entity.User;
import com.weiya.mapper.PermissionMapper;
import com.weiya.mapper.RoleMapper;
import com.weiya.mapper.RolePermissionMapper;
import com.weiya.mapper.UserMapper;
import com.weiya.response.ResponseUtil;
import com.weiya.response.ResponseVo;
import com.weiya.response.SystemConst;
import com.weiya.service.RoleService;
import com.weiya.util.UUIDUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p class="detail">
 * 功能:角色业务实现类
 * </p>
 *
 * @author BaoWeiwei
 * @ClassName Role service.
 * @Version V1.0.
 * @date 2018.07.18 11:45:29
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private PermissionMapper permissionMapper;
    @Autowired
    private RolePermissionMapper rolePermissionMapper;


    /**
     * 根据用户id查询当前用户角色
     *
     * @param userId
     * @return
     */
    @Override
    public Set<String> findRoleByUserId(String userId) {
        return roleMapper.findRoleByUserId(userId);
    }

    /**
     * 根据条件查询角色列表
     *
     * @param role
     * @return
     */
    @Override
    public List<Role> selectRoles(Role role) {
        return roleMapper.selectRoles(role);
    }

    /**
     * 新增角色
     *
     * @param role
     * @return
     */
    @Override
    public int insert(Role role) {
        role.setRoleId(UUIDUtil.getUniqueIdByUUId());
        role.setStatus(SystemConst.DATA_VALID);
        Date date = new Date();
        role.setCreateTime(date);
        role.setUpdateTime(date);
        return roleMapper.insert(role);
    }

    /**
     * 根据角色id查询角色详情
     *
     * @param roleId
     * @return
     */
    @Override
    public Role findById(String roleId) {
        Role role = new Role();
        role.setRoleId(roleId);
        return roleMapper.selectByPrimaryKey(roleId);
    }

    /**
     * 修改角色信息
     *
     * @param role
     * @return
     */
    @Override
    public int updateRole(Role role) {
        return roleMapper.updateRole(role);
    }

    /**
     * 批量删除角色
     *
     * @param roleIdsList
     * @param status
     * @return
     */
    @Override
    public int updateStatusBatch(List<String> roleIdsList, Integer status) {
        Map<String,Object> params = new HashMap<String,Object>(2);
        params.put("roleIds",roleIdsList);
        params.put("status",status);
        return roleMapper.updateStatusBatch(params);
    }

    /**
     * 查询当前角色所拥有的权限
     *
     * @param roleId
     * @return
     */
    @Override
    public List<Permission> findPermissionsByRoleId(String roleId) {
        return permissionMapper.findByRoleId(roleId);
    }

    /**
     * 根据角色id分配权限
     *
     * @param roleId
     * @param permissionIdsList
     * @return
     */
    @Override
    public ResponseVo addAssignPermission(String roleId, List<String> permissionIdsList) {
        try{
            RolePermission rolePermission = new RolePermission();
            rolePermission.setRoleId(roleId);
            rolePermissionMapper.delete(rolePermission);
            for(String permissionId : permissionIdsList){
                if(permissionId !="" ){
                    rolePermission.setPermissionId(permissionId);
                    rolePermissionMapper.insert(rolePermission);
                }
            }
            return ResponseUtil.success("分配权限成功!");
        }catch(Exception e){
            return ResponseUtil.error("分配权限失败!");
        }
    }

    /**
     * 根据角色id查询用户当前角色的所有用户
     *
     * @param roleId
     * @return
     */
    @Override
    public List<User> findByRoleId(String roleId) {
        return userMapper.findByRoleId(roleId);
    }
}
