package com.weiya.service.impl;

import com.weiya.entity.Permission;
import com.weiya.mapper.PermissionMapper;
import com.weiya.response.SystemConst;
import com.weiya.service.PermissionService;
import com.weiya.util.UUIDUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p class="detail">
 * 功能:权限资源业务实现类
 * </p>
 *
 * @author BaoWeiwei
 * @ClassName Permission service.
 * @Version V1.0.
 * @date 2018.07.18 11:46:05
 */
@Service
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;

    /**
     * 根据用户id查询当前用户拥有权限
     *
     * @param userId
     * @return
     */
    @Override
    public Set<String> findPermsByUserId(String userId) {
        return permissionMapper.findPermsByUserId(userId);
    }

    /**
     * 根据用户id查询当前用户拥有的菜单权限
     *
     * @param userId
     * @return
     */
    @Override
    public List<Permission> selectMenuByUserId(String userId) {
        return permissionMapper.selectMenuByUserId(userId);
    }

    /**
     * 查询所有有效权限
     *
     * @param status
     * @return
     */
    @Override
    public List<Permission> selectAll(Integer status) {
        return permissionMapper.selectAllPerms(status);
    }

    /**
     * 查询所有菜单资源
     *
     * @param status
     * @return
     */
    @Override
    public List<Permission> selectAllMenuName(Integer status) {
        return permissionMapper.selectAllMenuName(status);
    }

    /**
     * 根据id获取权限信息
     *
     * @param permissionId
     * @return
     */
    @Override
    public Permission findByPermissionId(String permissionId) {
        return permissionMapper.findByPermissionId(permissionId);
    }

    /**
     * 修改权限
     *
     * @param permission
     * @return
     */
    @Override
    public int updateByPermissionId(Permission permission) {
        return permissionMapper.updateByPermissionId(permission);
    }

    /**
     * 根据资源id查询下级资源
     *
     * @param permissionId
     * @return
     */
    @Override
    public int selectSubPermsByPermissionId(String permissionId) {
        return permissionMapper.selectSubPermsByPermissionId(permissionId);
    }

    /**
     * 删除资源，状态置为已删除
     *
     * @param permissionId
     * @param status
     * @return
     */
    @Override
    public int updateStatus(String permissionId, Integer status) {
        Map<String,Object> params = new HashMap<String,Object>(2);
        params.put("permissionId",permissionId);
        params.put("status",status);
        return permissionMapper.updateStatusByPermissionId(params);
    }

    /**
     * 新增资源
     *
     * @param permission
     * @return
     */
    @Override
    public int insert(Permission permission) {
        permission.setPermissionId(UUIDUtil.getUniqueIdByUUId());
        permission.setStatus(SystemConst.DATA_VALID);
        Date date = new Date();
        permission.setCreateTime(date);
        permission.setUpdateTime(date);
        return permissionMapper.insert(permission);
    }

    /**
     * 根据parentID查询当前目录下的所有资源
     *
     * @param parentId
     * @return
     */
    @Override
    public List<Permission> selectAllPermission(String parentId) {
        return permissionMapper.selectAllPermission(parentId);
    }

}
