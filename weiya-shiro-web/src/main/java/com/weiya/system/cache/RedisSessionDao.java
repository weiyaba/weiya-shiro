package com.weiya.system.cache;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.eis.AbstractSessionDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * <p class="detail">
 * 功能:使用redis来管理session信息
 * </p>
 *
 * @author BaoWeiwei
 * @ClassName Redis session dao.
 * @Version V1.0.
 * @date 2018.07.26 23:44:29
 */
public class RedisSessionDao extends AbstractSessionDAO {

	private static Logger logger = LoggerFactory.getLogger(RedisSessionDao.class);

	private static final String SHIRO_SESSION_PREFIX = "weiya-session:";

	private RedisTemplate redisTemplate;

	private ValueOperations valueOperations;

    /**
     * Instantiates a new Redis session dao.
     *
     * @param redisTemplate the redis template
     */
    public RedisSessionDao(RedisTemplate redisTemplate) {
		this.redisTemplate = redisTemplate;
		this.valueOperations = redisTemplate.opsForValue();
	}

    private String generateKey(Object key) {
        return SHIRO_SESSION_PREFIX + key;
    }

	@Override
	protected Serializable doCreate(Session session) {
		Serializable sessionId = this.generateSessionId(session);
		if (logger.isDebugEnabled()) {
            logger.debug("shiro redis session create. sessionId={"+sessionId+"}");
		}
		this.assignSessionId(session, sessionId);
		valueOperations.set(generateKey(sessionId), session, session.getTimeout(), TimeUnit.MILLISECONDS);
		return sessionId;
	}

	@Override
	protected Session doReadSession(Serializable sessionId) {
		if (logger.isDebugEnabled()) {
			logger.debug("shiro redis session read. sessionId={"+sessionId+"}");
		}
		return (Session) valueOperations.get(generateKey(sessionId));
	}

	@Override
	public void update(Session session) throws UnknownSessionException {
		if (logger.isDebugEnabled()) {
			logger.debug("shiro redis session update. sessionId={"+session.getId()+"}");
		}
		valueOperations.set(generateKey(session.getId()), session, session.getTimeout(), TimeUnit.MILLISECONDS);
	}

	@Override
	public void delete(Session session) {
		if (logger.isDebugEnabled()) {
			logger.debug("shiro redis session delete. sessionId={"+session.getId()+"}");
		}
		redisTemplate.delete(generateKey(session.getId()));
	}

	@Override
	public Collection<Session> getActiveSessions() {
		Set<Object> keySet = redisTemplate.keys(generateKey("*"));
		Set<Session> sessionSet = new HashSet<>();
		if (keySet==null||keySet.isEmpty()) {
			return Collections.emptySet();
		}
		for (Object key : keySet) {
			sessionSet.add((Session) valueOperations.get(key));
		}
		if (logger.isDebugEnabled()) {
			logger.debug("shiro redis session all. size={"+keySet.size()+"}");
		}
		return sessionSet;
	}

}
