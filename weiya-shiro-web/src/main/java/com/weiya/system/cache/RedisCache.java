package com.weiya.system.cache;

import com.weiya.entity.User;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Collection;
import java.util.Set;

/**
 * <p class="detail">
 * 功能:为shiro量身定做的一个redis cache,为Authorization cache做了特别优化
 * </p>
 *
 * @param <K> the type parameter
 * @param <V> the type parameter
 * @author BaoWeiwei
 * @ClassName Redis cache.
 * @Version V1.0.
 * @date 2018.07.26 23:40:18
 */
public class RedisCache<K, V> implements Cache<K, V> {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    private String cacheKey;

    /**
     * Instantiates a new Redis cache.
     *
     * @param cacheKey the cache key
     */
    public RedisCache(String cacheKey) {
        this.cacheKey = cacheKey;
    }

    @Override
    public V get(K key) throws CacheException {
        BoundHashOperations<String, K, V> hash = redisTemplate.boundHashOps(cacheKey);
        Object k = hashKey(key);
        return hash.get(k);
    }

    @Override
    public V put(K key, V value) throws CacheException {
        BoundHashOperations<String, K, V> hash = redisTemplate.boundHashOps(cacheKey);
        Object k = hashKey(key);
        hash.put((K) k, value);
        return value;
    }

    @Override
    public V remove(K key) throws CacheException {
        BoundHashOperations<String, K, V> hash = redisTemplate.boundHashOps(cacheKey);

        Object k = hashKey(key);
        V value = hash.get(k);
        hash.delete(k);
        return value;
    }

    @Override
    public void clear() throws CacheException {
        redisTemplate.delete(cacheKey);
    }

    @Override
    public int size() {
        BoundHashOperations<String, K, V> hash = redisTemplate.boundHashOps(cacheKey);
        return hash.size().intValue();
    }

    @Override
    public Set<K> keys() {
        BoundHashOperations<String, K, V> hash = redisTemplate.boundHashOps(cacheKey);
        return hash.keys();
    }

    @Override
    public Collection<V> values() {
        BoundHashOperations<String, K, V> hash = redisTemplate.boundHashOps(cacheKey);
        return hash.values();
    }

    /**
     * <p class="detail">
     * 功能:
     * </p>
     *
     * @param key :
     * @return object
     * @author BaoWeiwei
     * @date 2018.07.26 23:40:18
     */
    protected Object hashKey(K key) {
        //此处很重要,如果key是登录凭证,那么这是访问用户的授权缓存;
        // 将登录凭证转为user对象,返回user的id属性做为hash key,
        // 否则会以user对象做为hash key,这样就不好清除指定用户的缓存了
        if (key instanceof PrincipalCollection) {
            PrincipalCollection pc = (PrincipalCollection) key;
            User sysUser = (User) pc.getPrimaryPrincipal();
            return sysUser.getUserId();
        }
        return key;
    }
}

