package com.weiya.system.error;

/**
 * <p class="detail">
 * 功能:自定义异常
 * </p>
 *
 * @author BaoWeiwei
 * @ClassName Customer exception.
 * @Version V1.0.
 * @date 2018.07.27 14:16:11
 */
public class CustomException extends RuntimeException {

    private static final long serialVersionUID = -2779885858644535186L;

    private Integer code;
    private String msg;

    public CustomException(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "CustomException{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                '}';
    }
}
