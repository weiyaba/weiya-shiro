package com.weiya.system.error;

import com.weiya.response.ResponseCode;
import com.weiya.response.ResponseUtil;
import com.weiya.response.ResponseVo;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.expression.AccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * <p class="detail">
 * 功能:注解方式统一异常处理
 * </p>
 *
 * @author BaoWeiwei
 * @ClassName System global exception handler.
 * @Version V1.0.
 * @date 2018.08.10 11:41:07
 */
@RestControllerAdvice
public class SystemGlobalExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(SystemGlobalExceptionHandler.class);


    /**
     * <p class="detail">
     * 功能:参数异常
     * </p>
     *
     * @param e :
     * @return response vo
     * @author BaoWeiwei
     * @date 2018.08.10 11:41:07
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseVo handleMethodArgumentNotValidException(Exception e){
		return ResponseUtil.error(400,"参数验证失败");
	}

    /**
     * <p class="detail">
     * 功能:捕捉401
     * </p>
     *
     * @param e :
     * @return response vo
     * @author BaoWeiwei
     * @date 2018.08.10 11:41:07
     */
    @ExceptionHandler(UnauthorizedException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public ResponseVo handleNotAuthorizedException(Exception e) {
		logger.error(e.getMessage(), e);
		return ResponseUtil.error(ResponseCode.UNAUTHORIZED.getCode(),e.getLocalizedMessage());
	}

    /**
     * <p class="detail">
     * 功能:数据访问异常
     * </p>
     *
     * @param e :
     * @return response vo
     * @author BaoWeiwei
     * @date 2018.08.10 11:41:07
     */
    @ExceptionHandler(DataAccessException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseVo handleDataAccessException(Exception e) {
		logger.error(e.getMessage(), e);
		return ResponseUtil.error("数据访问错误: " + e.getLocalizedMessage());
	}


    /**
     * <p class="detail">
     * 功能:拦截捕捉处理自定义异常
     * </p>
     *
     * @param e :
     * @return response vo
     * @author BaoWeiwei
     * @date 2018.08.10 11:50:25
     */
    @ExceptionHandler(CustomException.class)
	public ResponseVo handlerCustomException(CustomException e){
        logger.error(e.getMessage(), e);
        return ResponseUtil.error(e.getCode(),e.getMsg());
    }


    /**
     * <p class="detail">
     * 功能:全局异常捕捉处理
     * </p>
     *
     * @param e :
     * @return response vo
     * @author BaoWeiwei
     * @date 2018.08.10 11:41:07
     */
    @ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseVo handleException(Exception e) {
		logger.error(e.getMessage(), e);
		return ResponseUtil.error("系统内部错误: " + e.getLocalizedMessage());
	}
}
