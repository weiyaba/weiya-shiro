package com.weiya.system.shiro;

import com.weiya.entity.User;
import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;


/**
 * <p class="detail">
 * 功能:对用户注册时提交密码加密
 * </p>
 *
 * @author BaoWeiwei
 * @ClassName Password helper.
 * @Version V1.0.
 * @date 2017.03.01 14:10:23
 */
public class PasswordHelper {

    private static RandomNumberGenerator randomNumberGenerator = new SecureRandomNumberGenerator();
    private static String algorithmName = "md5";
    private static int hashIterations = 2;

    /**
     * <p class="detail">
     * 功能:使用salt（随机数）+用户名进行3次MD5加密，返回user
     * </p>
     *
     * @param user :
     * @return account
     * @author BaoWeiwei
     * @date 2017.03.07 17:56:49
     */
    public static User encryptPassword(User user) {

        user.setSalt(randomNumberGenerator.nextBytes().toHex());

        String newPassword = new SimpleHash(
                algorithmName,
                user.getPassword(),
                ByteSource.Util.bytes(user.getCredentialsSalt()),
                hashIterations)
                .toHex();
        user.setPassword(newPassword);
        return user;
    }

    /**
     * <p class="detail">
     * 功能:使用salt（随机数）+用户名进行3次MD5加密
     * </p>
     *
     * @param user :
     * @return string
     * @author BaoWeiwei
     * @date 2017.07.17 17:26:55
     */
    public static String getPassword(User user){
        String encryptPassword = new SimpleHash(
                algorithmName,user.getPassword(),
                ByteSource.Util.bytes(user.getCredentialsSalt()),
                hashIterations)
                .toHex();
        return encryptPassword;
    }
}
