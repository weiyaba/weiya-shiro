package com.weiya.system.shiro;

import com.weiya.entity.User;
import com.weiya.response.SystemConst;
import com.weiya.service.PermissionService;
import com.weiya.service.RoleService;
import com.weiya.service.UserService;
import com.weiya.system.cache.RedisSessionDao;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.mgt.RealmSecurityManager;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.subject.support.DefaultSubjectContext;
import org.apache.shiro.util.ByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * <p class="detail">
 * 功能:自定义realm实现用户验证及授权
 * </p>
 *
 * @author BaoWeiwei
 * @ClassName System shiro realm.
 * @Version V1.0.
 * @date 2018.07.17 23:11:00
 */
public class SystemUserRealm extends AuthorizingRealm {

    private static final Logger logger = LoggerFactory.getLogger(SystemUserRealm.class);

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private RedisSessionDao redisSessionDao;
    /**
     * 授权
     * @param principals
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        if(principals == null){
            throw new AuthorizationException("principals should not be null");
        }
        User user = (User) principals.getPrimaryPrincipal();
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.setRoles(roleService.findRoleByUserId(user.getUserId()));
        info.setStringPermissions(permissionService.findPermsByUserId(user.getUserId()));
        return info;
    }

    /**
     * 认证
     * @param token
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String username = (String) token.getPrincipal();
        User user = userService.selectByUsername(username);
        if(user == null) {
            //没找到帐号
            throw new UnknownAccountException();
        }
        if (SystemConst.DATA_INVALID.equals(user.getStatus()) || SystemConst.DATA_DELETED.equals(user.getStatus())) {
            // 帐号锁定
            throw new LockedAccountException();
        }
        //交给 AuthenticatingRealm 使用 CredentialsMatcher 进行密码匹配
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
                // 用户名
                user,
                // 密码
                user.getPassword(),
                // salt=username+salt
                ByteSource.Util.bytes(user.getCredentialsSalt()),
                // realm name
                getName()
        );
        return authenticationInfo;
    }


    /**
     * <p class="detail">
     * 功能:清除认证信息
     * </p>
     *
     * @param userIds :
     * @author BaoWeiwei
     * @date 2018.07.25 22:05:51
     */
    public void clearCachedAuthenticationInfo(List<String> userIds) {
        if(null == userIds || userIds.size() == 0)	{
            return ;
        }
        List<SimplePrincipalCollection> list = getSimplePrincipalCollection(userIds);
        RealmSecurityManager securityManager =
                (RealmSecurityManager) SecurityUtils.getSecurityManager();
        SystemUserRealm realm = (SystemUserRealm)securityManager.getRealms().iterator().next();
        for (SimplePrincipalCollection simplePrincipalCollection : list) {
            realm.clearCachedAuthenticationInfo(simplePrincipalCollection);
        }
    }


    /**
     * <p class="detail">
     * 功能:
     * </p>
     *
     * @param userIds :
     * @author BaoWeiwei
     * @date 2018.07.25 22:05:36
     */
    public void clearAuthorizationByUserId(List<String> userIds){
        if(null == userIds || userIds.size() == 0)	{
            return ;
        }
        List<SimplePrincipalCollection> list = getSimplePrincipalCollection(userIds);
        RealmSecurityManager securityManager =
                (RealmSecurityManager) SecurityUtils.getSecurityManager();
        SystemUserRealm realm = (SystemUserRealm)securityManager.getRealms().iterator().next();
        for (SimplePrincipalCollection simplePrincipalCollection : list) {
            realm.clearCachedAuthorizationInfo(simplePrincipalCollection);
        }
    }


    /**
     * 根据用户id获取所有SimplePrincipalCollection
     * @param userIds 已经修改了权限的userId
     */
    private  List<SimplePrincipalCollection> getSimplePrincipalCollection(List<String> userIds){
        // 获取当前已登录的用户session列表
        Collection<Session> sessions = redisSessionDao.getActiveSessions();
        // 定义返回
        List<SimplePrincipalCollection> list = new ArrayList<SimplePrincipalCollection>();
        for (Session session:sessions){
            // 获取session登录信息
            Object obj = session.getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY);
            if(null != obj && obj instanceof SimplePrincipalCollection){
                //强转
                SimplePrincipalCollection spc = (SimplePrincipalCollection)obj;
                //判断用户，匹配用户ID。
                obj = spc.getPrimaryPrincipal();
                if(null != obj && obj instanceof User){
                    User user = (User) obj;
                    System.out.println("user:"+user);
                    //比较用户ID，符合即加入集合
                    if(null != user && userIds.contains(user.getUserId())){
                        list.add(spc);
                    }
                }
            }
        }
        return list;
    }
}
