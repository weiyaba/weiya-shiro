/*
 * 版权所有(C) 浙江大道网络科技有限公司2011-2020
 * Copyright 2009-2020 Zhejiang GreatTao Factoring Co., Ltd.
 *
 * This software is the confidential and proprietary information of
 * Zhejiang GreatTao Corporation ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Zhejiang GreatTao
 */

package com.weiya.system.shiro;

import com.weiya.entity.Permission;
import com.weiya.response.SystemConst;
import com.weiya.service.PermissionService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.config.Ini;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import com.google.common.io.CharStreams;
import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.List;

/**
 * <p class="detail">
 * 功能:重写ShiroFilterFactoryBean类中的setFilterChainDefinitions()方法
 * 从配置文件中读取过滤器规则
 * </p>
 *
 * @author BaoWeiwei
 * @ClassName Chain definition section meta source.
 * @Version V1.0.
 * @date 2018.07.26 15:05:58
 */
public class ChainDefinitionSectionMetaSource implements FactoryBean<Ini.Section> {


    private String filterChainDefinitionFile = "filter-chain-definition.properties";
    /**
     * The constant PREMISSION_STRING.
     */
    public static final String PREMISSION_STRING = "perms[\"{0}\"]";
    @Autowired
    private PermissionService permissionService;

    /**
     * 初始化权限
     * @return
     * @throws Exception
     */
    @Override
    public Ini.Section getObject() throws Exception {
        String definitions;
        if (filterChainDefinitionFile.startsWith("http")) {
            definitions = IOUtils.toString(new URL(filterChainDefinitionFile), StandardCharsets.UTF_8);
        } else {
            definitions = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream(filterChainDefinitionFile), Charset.forName("UTF-8")));InputStream in = this.getClass().getClassLoader().getResourceAsStream(filterChainDefinitionFile);
        }
        Ini ini = new Ini();
        ini.load(definitions);
        Ini.Section section = ini.getSection(Ini.DEFAULT_SECTION_NAME);
        List<Permission> permissionList = permissionService.selectAll(SystemConst.DATA_VALID);
        for (Permission permission : permissionList) {
            if (StringUtils.isNotBlank(permission.getUrl()) && StringUtils.isNotBlank(permission.getPerms())) {
                String perm = "perms[" + permission.getPerms() + "]";
                section.put(permission.getUrl(), MessageFormat.format(PREMISSION_STRING, permission.getPerms()));
            }
        }
        section.put("/**", "user");
        return section;
    }

    @Override
    public Class<?> getObjectType() {
        return this.getClass();
    }

    @Override
    public boolean isSingleton() {
        return false;
    }

    /**
     * Sets filter chain definition file.
     *
     * @param filterChainDefinitionFile the filter chain definition file
     */
    public void setFilterChainDefinitionFile(String filterChainDefinitionFile) {
        this.filterChainDefinitionFile = filterChainDefinitionFile;
    }
}
