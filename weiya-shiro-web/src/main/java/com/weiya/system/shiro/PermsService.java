package com.weiya.system.shiro;

import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Component;

/**
 * <p class="detail">
 * 功能:thymeleaf页面中，这样配置可以在js中使用shiro标签
 * </p>
 *
 * @author BaoWeiwei
 * @ClassName Perms service.
 * @Version V1.0.
 * @date 2018.07.27 13:09:20
 */
@Component("perms")
public class PermsService
{
    /**
     * <p class="detail">
     * 功能:判断是否拥有该权限
     * </p>
     *
     * @param permission :
     * @return boolean
     * @author BaoWeiwei
     * @date 2018.07.27 13:09:20
     */
    public boolean hasPerm(String permission)
    {
        return SecurityUtils.getSubject().isPermitted(permission);
    }
}