package com.weiya.system.shiro;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionListenerAdapter;

public class WeiyaSessionListener extends SessionListenerAdapter {
    /**
     * Notification callback that occurs when the corresponding Session has started.
     *
     * @param session the session that has started.
     */
    @Override
    public void onStart(Session session) {
        System.out.println("会话创建：" + session.getId());
    }
}
