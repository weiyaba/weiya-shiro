package com.weiya.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * <p class="detail">
 * 功能:异常controller
 * </p>
 *
 * @author BaoWeiwei
 * @ClassName Error controller.
 * @Version V1.0.
 * @date 2018.07.27 16:06:48
 */
@Controller
@RequestMapping("/error")
public class ErrorController {
    /**
     * <p class="detail">
     * 功能:404
     * </p>
     *
     * @param model :
     * @return string
     * @author BaoWeiwei
     * @date 2018.07.27 16:06:48
     */
    @RequestMapping(value = "/404", method = RequestMethod.GET)
    public String pageNotFound(Model model) {
        return "error/404";
    }

    /**
     * <p class="detail">
     * 功能:500
     * </p>
     *
     * @param model :
     * @return string
     * @author BaoWeiwei
     * @date 2018.07.27 16:06:48
     */
    @RequestMapping(value = "/500", method = RequestMethod.GET)
    public String internalError(Model model) {
        return "error/500";
    }

    /**
     * <p class="detail">
     * 功能:无权限
     * </p>
     *
     * @param model :
     * @return string
     * @author BaoWeiwei
     * @date 2018.07.27 16:06:49
     */
    @RequestMapping(value = "/401", method = RequestMethod.GET)
    public String noPermission(Model model) {
        return "error/401";
    }
}
