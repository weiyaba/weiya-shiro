package com.weiya.controller;

import com.weiya.response.SystemConst;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * <p class="detail">
 * 功能:系统启动访问
 * </p>
 *
 * @author BaoWeiwei
 * @ClassName Index controller.
 * @Version V1.0.
 * @date 2018.06.14 14:44:54
 */
@Controller
public class WeiyaController {

    @Autowired
    private LocaleResolver localeResolver;


    /**
     * <p class="detail">
     * 功能:进入首页
     * </p>
     *
     * @return string
     * @author BaoWeiwei
     * @date 2018.06.14 14:45:00
     */
    @GetMapping(value = "/")
    public String welcome(){
        return "system/welcome";
    }

    /**
     * <p class="detail">
     * 功能:进入后台首页
     * </p>
     *
     * @return string
     * @author BaoWeiwei
     * @date 2018.07.16 16:26:54
     */
    @GetMapping(value = "/index")
    public String index(){
        return "system/index";
    }

    /**
     * <p class="detail">
     * 功能:后台首页
     * </p>
     *
     * @return string
     * @author BaoWeiwei
     * @date 2018.07.22 15:30:01
     */
    @GetMapping(value = {"/overview","/overview/{lang}"})
    public String overview(HttpServletRequest request, HttpServletResponse response,
                           @PathVariable(value = "lang",required = false) String lang ){
        if(SystemConst.LANG_EN.equals(lang) && StringUtils.isNotBlank(lang)){
            localeResolver.setLocale(request, response,Locale.ENGLISH);
        }else if(SystemConst.LANG_ZH.equals(lang) && StringUtils.isNotBlank(lang)){
            localeResolver.setLocale(request, response, Locale.CHINESE);
        } else {
            localeResolver.setLocale(request, response, LocaleContextHolder.getLocale());
        }
        return "system/overview";
    }

    /**
     * <p class="detail">
     * 功能:用户管理
     * </p>
     *
     * @return string
     * @author BaoWeiwei
     * @date 2018.07.22 15:31:05
     */
    @GetMapping("/users")
    public String userPage(){
        return "user/list";
    }

    /**
     * <p class="detail">
     * 功能:角色管理
     * </p>
     *
     * @return string
     * @author BaoWeiwei
     * @date 2018.07.24 13:29:35
     */
    @GetMapping("/roles")
    public String rolePage(){
        return "role/list";
    }

    @GetMapping("/permissions")
    public String permissionPage(){
        return "permission/list";
    }


    /**
     * <p class="detail">
     * 功能:图标工具
     * </p>
     *
     * @return string
     * @author BaoWeiwei
     * @date 2018.07.22 18:02:51
     */
    @GetMapping(value = "/icons")
    public String getIcons(){
        return "tool/icons";
    }
}
