package com.weiya.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/database")
public class DruidController {

    @GetMapping(value = "/monitoring")
    public String monitoring(){
        return "database/monitoring";
    }
}
