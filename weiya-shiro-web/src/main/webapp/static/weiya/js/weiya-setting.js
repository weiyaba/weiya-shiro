$(function () {
    //修改信息
    $("#userInfoBtn").on('click', function () {
        Global.load("#userInfoWindow", "/user/edit/" + $("#userId").text(), function () {
            $("#userInfoModal").modal("show");
        });
    })
    /*修改密码*/
    $("#changePasswordBtn").on('click', function () {
        $("#passwordForm")[0].reset();
        Global.clearError("#passwordForm");
    })
    /*确定修改密码*/
    $("#savePassword").on('click', function () {
        if (doValidForm(passwordForm)) {
            Global.mask("#savePassword")
            Global.postAjax(
                "/user/password/change",
                $("#passwordForm").serialize(),
                function (data) {
                    Global.unmask("#savePassword")
                    if (data.code == 200) {
                        $("#passwordModal").modal("hide");
                        layer.msg('密码修改成功!');
                    } else {
                        layer.msg(data.msg);
                    }
                })
        }
    })

    $("#passwordForm").keyup(function (event) {
        if (event.keyCode == 13) {
            $("#savePassword").click();
        }
    });

    $(".system-info,.system-notice").on('click', function () {
        layer.msg('该功能尚未开通，敬请期待!');
    });
});