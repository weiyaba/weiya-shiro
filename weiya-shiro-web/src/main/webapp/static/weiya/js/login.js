$(function () {
    // 点击图片刷新验证码
    $(".verificationCode").on('click', function () {
        $(this).hide().attr('src', '/captchaCode?' + Math.floor(Math.random() * 100)).fadeIn();
    });

    // 注册
    $("#registerBtn").on('click', function () {
        if (!doValidForm(registerForm)) {
            //没有通过验证
            return;
        }
        var options = {
            type : "POST",
            success: function(data){
                console.log(data);
                if (data.code == 200) {
                    layer.msg(
                        data.msg,
                        {
                            offset: '30%',
                            time: 800
                        },
                        function () {
                            window.location.href = "/login";
                        });
                }else {
                    layer.msg(data.msg);
                    $("#verificationCode").click();
                }
            }
        };
        $("#registerForm").ajaxSubmit(options);
    });

    // 登录
    $("#loginBtn").on('click', function () {
        if (!doValidForm(loginForm)) {
            //没有通过验证
            return;
        }

        var options = {
            type: "POST",
            success: function (data) {
                if (data.code == 200) {
                    layer.msg(data.msg, {
                        offset: '30%',
                        time: 800
                    }, function () {
                        window.location.href = "/index";
                    });
                }else{
                    layer.msg(data.msg, {
                        icon: 2,
                        offset: '30%',
                        time: 2000
                    });
                    $("#verificationCode").click();
                }
            }
        };
        $("#loginForm").ajaxSubmit(options);
    });


    // 按Enter键提交表单
    $(document).keyup(function (event) {
        if (event.keyCode == 13) {
            var currHref = window.location.href;
            if (currHref.indexOf("register") > -1) {
                $("#registerBtn").click();
            } else {
                $("#loginBtn").click();
            }
        }
    });

    // 第三方登录暂时不做
    $(".third-qq,.third-weixin,.third-weibo").on('click', function () {
        layer.msg("别逗了,你见过后台有第三方登录的吗?我就是为了样式好看，哈哈!",{time:3000})
    });

});