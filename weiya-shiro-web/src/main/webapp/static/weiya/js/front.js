/**
 * Created by Baoweiwei on 2018/6/21.
 */

$(function () {
    $(window).scroll(function () {
        if ($(window).scrollTop() > 80) {
            $("#toTop").show();
            $(".header").addClass("header-fixed");
        } else {
            $("#toTop").hide();
            $(".header").removeClass("header-fixed");
        }
    });

    $("#owl-demo").owlCarousel({
        items : 5,
        lazyLoad : false,
        // autoPlay: 3000,
        autoPlay : true,
        navigation : false,
        navigationText :  false,
        pagination : false
    });

    $("#owl-demo1").owlCarousel({
        items : 1,
        lazyLoad : false,
        autoPlay : true,
        navigation : false,
        navigationText :  false,
        pagination : true
    });

    $("#toTop").on('click',function () {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;

    });

    $("nav li").click(function() {//导航点击
        $("nav a").removeClass("actived");
        $(this).children("a").addClass("actived");
    });


});