//@ sourceURL=weiya-permission.js
$(function () {

    // 初始化表格列属性
    Menu.initColumn = function () {
        var columns = [
            {
                field: 'selectItem',
                radio: true
            },
            {
                title: '菜单名称',
                field: 'name',
                align: 'left'
            },
            {
                title: '菜单URL',
                field: 'url',
                align: 'left'
            },
            {
                title: '权限标识',
                field: 'perms',
                align: 'center'
            },
            {
                title: '类型',
                field: 'type',
                align: 'center',
                width: '60px',
                formatter: function (row, index) {
                    switch (row.type) {
                        case 0 :
                            return '<span class="label label-primary">目录</span>';
                        case 1 :
                            return '<span class="label label-success">菜单</span>';
                        case 2 :
                            return '<span class="label label-warning">按钮</span>';
                    }
                }
            },
            {
                title: '图标',
                field: 'icon',
                align: 'center',
                width: '60px',
                formatter: function (row, index) {
                    if (row.type != 2) {
                        return '<i class="' + row.icon + ' fa-lg"></i>';
                    } else {
                        return "";
                    }
                }
            },
            {
                title: '排序',
                field: 'orderNum',
                align: 'center',
                width: '60px'
            },
            {
                title: '权限描述',
                field: 'description',
                align: 'left'
            },
            {
                field: 'operation',
                title: '操作',
                align: "center",
                formatter: function (row, index) {
                    var edit = editFlag=="true" ? '<a class="table-btn table-btn-info" href="javascript:void(0)" onclick="editPermission(' + row.permissionId + ')">编辑</a>' : '';
                    var del = deleteFlag=="true" ? '<a  class="table-btn table-btn-danger" href="javascript:void(0)" onclick="deletePermission(' + row.permissionId + ')">删除</a>' : '';
                    return edit + del;
                }
            }]
        return columns;
    };

    // 初始化资源列表
    initPermissionTable(Menu);

    // 新增资源单选按钮选中事件
    $('input:radio[name="type"]').on("click", function () {
        initPermissionForm();
        Global.clearError("#permissionForm");
        menuType = $('input:radio[name="type"]:checked').val();
        if (menuType == "0") {
            $(".btnFlag").hide();
            $(".menuFlag").hide();
            $(".catalogFlag").show();
        }
        if (menuType == "1") {
            $(".menuFlag").show();
        }
        if (menuType == "2") {
            $(".catalogFlag").hide();
            $(".menuFlag").hide();
            $(".btnFlag").show();
        }
    });

    // 保存新增资源
    $("#savePermission").on("click", function () {
        if ((menuType == "0" || menuType == "1") && menuCheckedType != 0) {
            layer.msg("上级资源只能是目录类型！", function () {});
            return;
        } else if (menuType == "2" && menuCheckedType != 1) {
            layer.msg("上级资源只能是菜单类型！", function () {});
            return;
        }
        if (doValidForm(permissionForm)) {
            if (menuType == "2") {
                $("#orderNum").val(0);
            }
            Global.mask("#savePermission");
            Global.postAjax(
                "/permission/add",
                $("#permissionForm").serialize(),
                function (data) {
                    Global.unmask("#savePermission");
                    if (data.code == 200) {
                        $("#permissionModal").modal("hide");
                        Menu.table.refresh();
                    }
                    layer.msg(data.msg);
                });

        }
    });
    // 新增权限modal框关闭后清空数据
    $("#permissionModal").on('hidden.bs.modal', function () {
        initPermissionForm();
        $("#type-menu").click();
        $("#parentMenu").val("顶层菜单");
        $("#parentId").val(0);
        $(".menuFlag").show();
        menuType = "1";
        menuCheckedType = 0;
    });


});

// 初始化权限列表
function initPermissionTable(Menu) {
    var colunms = Menu.initColumn();
    var table = new TreeTable(Menu.id, "/permission/list", colunms, "permissionId");
    table.setExpandColumn(1);
    table.setIdField("permissionId");
    table.setCodeField("permissionId");
    table.setParentCodeField("parentId");
    /*table.setExpandAll(true);*/
    table.setStriped(false);
    table.init();
    Menu.table = table;
}

// 修改权限
function editPermission(permissionId) {
    Global.load("#permissionOpenWindow", "/permission/edit/" + permissionId, function () {
        $("#permissionDetailModal").modal("show");
    }, 2);
}

// 删除权限
function deletePermission(permissionId) {
    Global.confirm("确认删除该权限？", function () {
        Global.postAjax(
            '/permission/delete',
            {"permissionId": permissionId},
            function (data) {
                if (data.code == 200) {
                    Menu.table.refresh();
                }
                layer.msg(data.msg);
            });
    })
}

// 初始化新增资源表单弹出框，即清空操作
function initPermissionForm() {
    $("#name").val("");
    $("#url").val("");
    $("#perms").val("");
    $("#orderNum").val("");
    $("#icon").val("");
    $("#description").val("");
}


function getMenuId() {
    var selected = $('#menuTable').bootstrapTreeTable('getSelections');
    if (selected.length == 0) {
        layer.msg("请选择一条记录");
        return false;
    } else {
        return selected[0].id;
    }
}

/*选择上级菜单树*/
function showMenu(flag) {
    var ztree;
    //树-初始化设置
    var treeSetting = {
        data: {
            simpleData: {
                enable: true,
                idKey: "permissionId",
                pIdKey: "parentId"
            }
        }
    };
    Global.postAjax(
        '/permission/list',
        {flag: "2"},
        function (data) {
            var topMenu = {
                name: "顶层菜单",
                permissionId: 0,
                parentId: "",
                type: 0
            }
            data.unshift(topMenu);
            $.each(data, function (i, item) {
                item.open = true;
            })
            ztree = $.fn.zTree.init($("#permissionTree"), treeSetting, data);
            layer.open({
                type: 1,
                offset: '50px',
                skin: 'layui-layer-molv',
                title: "选择菜单",
                area: ['300px', '450px'],
                /* shade: 0,
                 shadeClose: false,*/
                content: jQuery("#menuLayer"),
                btn: ['确定', '取消'],
                btn1: function (index) {
                    var node = ztree.getSelectedNodes();
                    if (node.length == 0) {
                        layer.msg("请选择一个菜单！", function () {
                        });
                        return;
                    }
                    if (flag == 1) {
                        menuCheckedType = node[0].type;
                        //选择上级菜单
                        $("#parentId").val(node[0].permissionId);
                        $("#parentMenu").val(node[0].name);
                    } else if (flag == 2) {
                        detailMenuCheckedType = node[0].type;
                        //选择上级菜单
                        $("#detailParentId").val(node[0].permissionId);
                        $("#detailParentMenu").val(node[0].name);
                    }
                    layer.close(index);
                }
            });
        });
}