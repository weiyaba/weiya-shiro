//@ sourceURL=weiya-role.js
$(function () {
    var columns = [
        {checkbox: true},
        {
            field: 'name',
            title: '角色名称',
            align: "center"
        }, {
            field: 'description',
            title: '角色描述',
            align: "center"
        },
        {
            field: 'createTime',
            title: '创建时间',
            align: "center",
            formatter: function (value, row, index) {
                return Global.longMsTimeConvertToDateTime(value);
            }
        },
        {
            field: 'operation',
            title: '操作',
            align: "center",
            formatter: function (value, row, index) {
                var edit = editFlag=="true" ? '<a class="table-btn table-btn-info" href="javascript:void(0)" onclick="editRole(' + row.roleId + ')">编辑</a>' : '';
                var assign = assignPermsFlag=="true" ?'<a class="table-btn table-btn-info"  href="javascript:void(0)" onclick="assignPermsList(' + row.roleId + ')">分配权限</a>' : '';
                var del = deleteFlag=="true" ? '<a  class="table-btn table-btn-danger" href="javascript:void(0)" onclick="deleteRole(' + row.roleId + ',' + row.status + ')">删除</a>' : '';
                return edit + assign + del;
            }
        }];
    var options = {
        id: "#table",
        url: '/role/list',
        columns: columns,
        toolbar: '#toolbar',
        showRefresh: true,
        queryParams: queryParams
    }

    // 查询参数
    function queryParams(params) {
        var temp = { //这里的键的名字和控制器的变量名必须一致，这边改动，控制器也需要改成一样的
            limit: params.limit, //页面大小
            offset: params.offset, //页码
            name: $("#roleName").val()
        };
        return temp;
    }

    // 初始化角色列表
    Global.initTable(options);

    // 点击查询按钮
    $("#queryBtn").click(function () {
        Global.refreshTable("#table");
    });
    // 点击重置按钮
    $("#resetBtn").click(function () {
        // $("#queryForm")[0].reset();
        Global.clearForm("queryForm");
    });

    // 新增角色
    $("#confirmAddBtn").click(function () {
        if (doValidForm(roleForm)) {
            Global.mask("#confirmAddBtn");
            Global.postAjax(
                "/role/add",
                $("#roleForm").serialize(),
                function (data) {
                    Global.unmask("#confirmAddBtn");
                    if (data.code == 200) {
                        $("#roleModal").modal("hide");
                        $("#roleForm")[0].reset();
                        Global.refreshTable("#table")
                    }
                    layer.msg(data.msg);
                })
        }
    });

    // 批量删除角色
    $("#batch-delete-btn").click(function () {
        var checkedRows = Global.selectMutiData("#table");
        if (checkedRows) {
            var result = true;
            $.each(checkedRows, function (i, item) {
                if (item.status == 3) {
                    layer.msg("请勿重复删除数据!");
                    result = false;
                }
            });
            if (!result) {
                return; // 该return只会跳出方法
            }
            Global.confirm("确定删除选中的" + checkedRows.length + "条记录？", function () {
                var roleIdStr = "";
                $.each(checkedRows, function (i, item) {
                    roleIdStr += (item.roleId + ",");
                })
                roleIdStr = roleIdStr.substring(0, roleIdStr.length - 1);
                Global.postAjax(
                    "/role/batch/delete",
                    {"roleIdStr": roleIdStr},
                    function (data) {
                        if (data.code == 200) {
                            Global.refreshTable("#table");
                        }
                        layer.msg(data.msg);
                    }, "get")
            })
        }
    });

    // 分配权限
    $("#saveAssign").click(function () {
        Global.mask("#saveAssign")
        var permissionIdStr = "";
        var treeObj = $.fn.zTree.getZTreeObj("permissionTree");
        var nodes = treeObj.getCheckedNodes(true);
        for (var i = 0; i < nodes.length; i++) {
            if (i == (nodes.length - 1)) {
                permissionIdStr += nodes[i].permissionId;
            } else {
                permissionIdStr += nodes[i].permissionId + ",";
            }
        }
        Global.postAjax(
            '/role/assign/permission',
            {
                roleId: roleId,
                permissionIdStr: permissionIdStr
            },
            function (data) {
                Global.unmask("#saveAssign");
                if (data.code == 200) {
                    $("#assignPermissionModal").modal("hide");
                }
                layer.msg(data.msg);
            });
    });

});

// 修改角色
function editRole(id) {
    Global.load("#roleOpenWindow", "/role/edit/" + id, function () {
        $("#roleDetailModal").modal("show");
    }, 2);
}

// 删除角色
function deleteRole(roleId,status) {
    if (status == 3) {
        layer.msg("请勿重复删除数据!");
        return;
    }
    Global.confirm("确定删除该角色？", function () {
        Global.postAjax(
            "/role/delete",
            {"roleId": roleId},
            function (data) {
                if (data.code == 200) {
                    Global.refreshTable("#table");
                }
                layer.msg(data.msg);
            }, "get")
    })
}

var roleId="";
// 分配权限，初始化权限列表
function assignPermsList(id) {
    roleId = id;
    Global.postAjax(
        "/role/assign/permission/list",
        {"roleId": roleId},
        function (data) {
            var zNodes = data;
            $.fn.zTree.init($("#permissionTree"), treeSetting, zNodes);
            $("#assignPermissionModal").modal("show");
        })
}

// 树-初始化设置
var treeSetting = {
    check: {
        enable: true
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "permissionId",
            pIdKey: "parentId"
        }
    }
};

