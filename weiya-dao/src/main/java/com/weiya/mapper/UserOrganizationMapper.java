package com.weiya.mapper;

import com.weiya.entity.UserOrganization;
import com.weiya.model.WeiyaMapper;

public interface UserOrganizationMapper extends WeiyaMapper<UserOrganization> {
}