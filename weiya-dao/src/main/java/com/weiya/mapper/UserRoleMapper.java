package com.weiya.mapper;

import com.weiya.entity.UserRole;
import com.weiya.model.WeiyaMapper;

public interface UserRoleMapper extends WeiyaMapper<UserRole> {
}