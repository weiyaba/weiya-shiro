package com.weiya.mapper;

import com.weiya.entity.Role;
import com.weiya.model.WeiyaMapper;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface RoleMapper extends WeiyaMapper<Role> {
    /**
     * 根据用户id查询当前用户角色
     * @param userId
     * @return
     */
    Set<String> findRoleByUserId(String userId);

    /**
     * 根据条件查询角色列表
     * @param role
     * @return
     */
    List<Role> selectRoles(Role role);

    /**
     * 根据角色id查询角色详情
     *
     * @param roleId
     * @return
     */
//    Role findById(String roleId);

    /**
     * 修改角色信息
     * @param role
     * @return
     */
    int updateRole(Role role);

    /**
     * 批量删除角色
     * @param params
     * @return
     */
    int updateStatusBatch(Map<String,Object> params);
}