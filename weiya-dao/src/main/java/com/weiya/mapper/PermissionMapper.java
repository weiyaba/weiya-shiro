package com.weiya.mapper;

import com.weiya.entity.Permission;
import com.weiya.model.WeiyaMapper;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Baoweiwei
 */
public interface PermissionMapper extends WeiyaMapper<Permission> {
    /**
     * 根据用户id查询当前用户拥有权限
     * @param userId
     * @return
     */
    Set<String> findPermsByUserId(String userId);

    /**
     * 根据用户id查询当前用户拥有的菜单权限
     *
     * @param userId
     * @return
     */
    List<Permission> selectMenuByUserId(String userId);

    /**
     * 查询所有有效权限
     * @param status
     * @return
     */
    List<Permission> selectAllPerms(Integer status);

    /**
     * 查询当前角色所拥有的权限
     * @param roleId
     * @return
     */
    List<Permission> findByRoleId(String roleId);

    /**
     * 查询所有菜单资源
     * @param status
     * @return
     */
    List<Permission> selectAllMenuName(Integer status);

    /**
     * 根据id获取权限信息
     * @param permissionId
     * @return
     */
    Permission findByPermissionId(String permissionId);

    /**
     * 修改权限
     * @param permission
     * @return
     */
    int updateByPermissionId(Permission permission);

    /**
     * 根据资源id查询是否下级资源
     * @param permissionId
     * @return
     */
    int selectSubPermsByPermissionId(String permissionId);

    /**
     * 删除资源，状态置为已删除
     * @param params
     * @return
     */
    int updateStatusByPermissionId(Map<String,Object> params);

    /**
     * 根据parentID查询当前目录下的所有资源
     * @param parentId
     * @return
     */
    List<Permission> selectAllPermission(String parentId);
}