package com.weiya.mapper;

import com.weiya.entity.User;
import com.weiya.model.WeiyaMapper;
import com.weiya.response.ResponseVo;

import java.util.List;
import java.util.Map;

public interface UserMapper extends WeiyaMapper<User> {
    /**
     * 根据用户名查询用户
     * @param username
     * @return
     */
    User selectByUsername(String username);

    /**
     * 更新最后登录时间
     * @param user
     */
    void updateLastLoginTime(User user);

    /**
     * 根据搜索条件查询所有用户
     * @param user
     * @return
     */
    List<User> selectUsers(User user);

    /**
     * 根据id批量删除用户
     * @param params
     * @return
     */
    int updateStatusBatch(Map<String,Object> params);

    /**
     * 根据用户id查询用户详情
     * @param userId
     * @return user
     */
    User selectByUserId(String userId);

    /**
     * 根据用户id更新用户信息
     * @param user
     * @return int
     */
    int updateByUserId(User user);

    /**
     * 根据角色id查询用户当前角色的所有用户
     * @param roleId
     * @return
     */
    List<User> findByRoleId(String roleId);
}