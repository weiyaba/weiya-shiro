package com.weiya.mapper;

import com.weiya.entity.Organization;
import com.weiya.model.WeiyaMapper;

public interface OrganizationMapper extends WeiyaMapper<Organization> {
}