package com.weiya.mapper;

import com.weiya.entity.RolePermission;
import com.weiya.model.WeiyaMapper;

public interface RolePermissionMapper extends WeiyaMapper<RolePermission> {
}